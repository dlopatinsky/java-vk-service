# VK Service

This service for VK responds to a request containing an HTTP header vk_service_token and a JSON body of user and group ids:
```json
{
  "user_id": ...,
  "group_id": ...
}
```
with a JSON telling the user's full name and whether the user is a member of the group:
```json
{
  "last_name": "...",
  "first_name": "...",
  "middle_name": "...",
  "member": true
}
```
