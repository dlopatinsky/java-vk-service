package net.dlopatinsky.vkservice.service;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.account.responses.GetProfileInfoResponse;
import com.vk.api.sdk.objects.groups.responses.IsMemberResponse;
import net.dlopatinsky.vkservice.pojo.UserAndGroupIds;
import net.minidev.json.JSONObject;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@CacheConfig(cacheNames = "apiData")
public class VkApiService {

    private final VkApiClient vk = new VkApiClient(new HttpTransportClient());

    @Cacheable(key = "#userAndGroupIds.userId + #userAndGroupIds.getGroupId()")
    public ResponseEntity<String> getCachedResponse(String token, UserAndGroupIds userAndGroupIds) {
        return getResponse(token, userAndGroupIds);
    }

    @CachePut(key = "#userAndGroupIds.userId + #userAndGroupIds.getGroupId()")
    public ResponseEntity<String> updateCachedResponse(String token, UserAndGroupIds userAndGroupIds) {
        return getResponse(token, userAndGroupIds);
    }

    @CacheEvict(allEntries = true)
    public void evictAll() {}

    private ResponseEntity<String> getResponse(String token, UserAndGroupIds userAndGroupIds) {
        if (!validateToken(token)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid token.");
        }

        int userId = userAndGroupIds.getUserId();
        String groupId = String.valueOf(userAndGroupIds.getGroupId());

        UserActor actor = new UserActor(userId, token);
        GetProfileInfoResponse profileInfoResponse = getProfileInfoResponse(actor);
        IsMemberResponse memberResponse = getMemberResponse(actor, groupId);

        String lastName = profileInfoResponse.getLastName();
        String firstName = profileInfoResponse.getFirstName();
        String middleName = profileInfoResponse.getMaidenName();
        boolean isMember = Integer.parseInt(memberResponse.getValue()) != 0;

        return getResponseEntity(lastName, firstName, middleName, isMember);
    }

    // Invalid user or group IDs are
    // kept in check when parsing the
    // JSON into UserAndGroupIds object
    private boolean validateToken(String token) {
        return token.matches("vk1\\.a\\.[a-zA-Z0-9-_]+");
    }

    private GetProfileInfoResponse getProfileInfoResponse(UserActor actor) {
        try {
            return vk.account().getProfileInfo(actor).execute();
        } catch (ApiException e) {
            throw getExceptionByCode(e.getCode());
        } catch (ClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private IsMemberResponse getMemberResponse(UserActor actor, String groupId) {
        try {
            return vk.groups().isMember(actor, groupId).execute();
        } catch (ApiException e) {
            throw getExceptionByCode(e.getCode());
        } catch (ClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseStatusException getExceptionByCode(int code) {
        switch (code) {
            case 5 -> {
                return new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token.");
            }
            case 6, 9, 14, 29 -> {
                return new ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS);
            }
            case 7, 15, 16, 21, 23 -> {
                return new ResponseStatusException(HttpStatus.FORBIDDEN, "Access denied.");
            }
            case 18 -> {
                return new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Page has been deleted.");
            }
            case 30 -> {
                return new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User profile is private.");
            }
            case 100 -> {
                return new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            default -> {
                return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error.");
            }
        }
    }

    private ResponseEntity<String> getResponseEntity(String lastName, String firstName,
                                                     String middleName, boolean isMember) {
        return new ResponseEntity<>(getResponseBody(lastName, firstName,
                middleName, isMember), HttpStatus.OK);
    }

    private String getResponseBody(String lastName, String firstName,
                                   String middleName, boolean isMember) {
        JSONObject responseBody = new JSONObject();
        responseBody.put("last_name", lastName);
        responseBody.put("first_name", firstName);
        responseBody.put("middle_name", middleName);
        responseBody.put("member", isMember);
        return responseBody.toString();
    }
}
