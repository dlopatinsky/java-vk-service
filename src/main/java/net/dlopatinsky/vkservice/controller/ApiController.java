package net.dlopatinsky.vkservice.controller;

import net.dlopatinsky.vkservice.pojo.UserAndGroupIds;
import net.dlopatinsky.vkservice.service.VkApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    VkApiService apiService;

    @GetMapping(value = "/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getFullNameAndMembershipStatus(@RequestHeader("vk_service_token") String token,
                                                     @RequestBody UserAndGroupIds userAndGroupIds) {
        return apiService.getCachedResponse(token, userAndGroupIds);
    }

    @GetMapping(value = "/update",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateFullNameAndMembershipStatus(@RequestHeader("vk_service_token") String token,
                                                     @RequestBody UserAndGroupIds userAndGroupIds) {
        return apiService.updateCachedResponse(token, userAndGroupIds);
    }

    @GetMapping("/evict")
    public void evictFullNameAndMembershipStatus() {
        apiService.evictAll();
    }
}
