package net.dlopatinsky.vkservice.pojo;

public class UserAndGroupIds {

    private final int userId;
    private final int groupId;

    public UserAndGroupIds (int user_id, int group_id) {
        this.userId = user_id;
        this.groupId = group_id;
    }

    public int getUserId() {
        return userId;
    }

    public int getGroupId() {
        return groupId;
    }
}
