package net.dlopatinsky.vkservice.service;

import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class VkApiServiceTest {

    @Test
    public void testGetResponseBodyNormalValuesTrueIsMember() {
        testGetResponseBody("Иванов", "Иван", "Иванович", true);
    }

    @Test
    public void testGetResponseBodyNormalValuesFalseIsMember() {
        testGetResponseBody("Иванов", "Иван", "Иванович", false);
    }

    @Test
    public void testGetResponseBodyNullMiddleNameTrueIsMember() {
        testGetResponseBody("Иванов", "Иван", null, true);
    }

    @Test
    public void testGetResponseBodyNullMiddleNameFalseIsMember() {
        testGetResponseBody("Иванов", "Иван", null, false);
    }

    @Test
    public void testValidateTokenNormalValue() {
        String testToken = "vk1.a.8szjtCxSg0M4DGbXvDOssYqXzdm6tPNWPm3RiMIeSAK7kv9wKQNOH7IZNJMzUUEmGZqNCv3nSQQLa3HLcA1ivZiOWxInp5sESj0RyxjGBq_Jm6KnLPPGRk0_qloHr1ImyBVgAverWZ7BUjRHft_XpuIgWHlvIuZuOBgaVP_4beEBgcX66C50SZNCr1lOq8T6nHa-vkPfGw4BxbykfLmSqw";
        boolean result = getValidateTokenResult(testToken);
        assert (result);
    }

    @Test
    public void testValidateTokenInvalidPrefix() {
        String testToken = "vk.8szjtCxSg0M4DGbXvDOssYqXzdm6tPNWPm3RiMIeSAK7kv9wKQNOH7IZNJMzUUEmGZqNCv3nSQQLa3HLcA1ivZiOWxInp5sESj0RyxjGBq_Jm6KnLPPGRk0_qloHr1ImyBVgAverWZ7BUjRHft_XpuIgWHlvIuZuOBgaVP_4beEBgcX66C50SZNCr1lOq8T6nHa-vkPfGw4BxbykfLmSqw";
        boolean result = getValidateTokenResult(testToken);
        assert (!result);
    }

    @Test
    public void testValidateTokenInvalidCharacters() {
        String testToken = "vk1.a.8szjtCxSg0M4DGbXvDOssYqXzdm6tPNWPm3RiMIeSAK7kv9wKQNOH7IZNJMzUUEmGZqNCv3nSQQLa3HLcA1ivZiOWxInp5sESj0RyxjGBq_Jm6KnLPPGRk0_qloHr1ImyBVgAverWZ7BUjRHft_XpuIgWHlvIuZuOBgaVP_4beEBgcX66C50SZNCr1lOq8T6nHa-vkPfGw4kfLm#*%2";
        boolean result = getValidateTokenResult(testToken);
        assert (!result);
    }

    @Test
    public void testValidateTokenEmptyString() {
        String testToken = "";
        boolean result = getValidateTokenResult(testToken);
        assert (!result);
    }

    private void testGetResponseBody(String testLastName, String testFirstName, String testMiddleName, boolean testIsMember) {
        VkApiService apiService = new VkApiService();
        Method method = null;
        try {
            method = VkApiService.class.getDeclaredMethod("getResponseBody", String.class, String.class, String.class, boolean.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(method).setAccessible(true);

        JSONObject expectedResult = new JSONObject();
        expectedResult.put("last_name", testLastName);
        expectedResult.put("first_name", testFirstName);
        expectedResult.put("middle_name", testMiddleName);
        expectedResult.put("member", testIsMember);
        Object result = null;
        try {
            result = method.invoke(apiService, testLastName, testFirstName, testMiddleName, testIsMember);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        assertEquals(result, expectedResult.toString());
    }

    private boolean getValidateTokenResult(String token) {
        VkApiService apiService = new VkApiService();
        Method method = null;
        try {
            method = VkApiService.class.getDeclaredMethod("validateToken", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(method).setAccessible(true);

        boolean result = false;
        try {
            result = (boolean) method.invoke(apiService, token);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return result;
    }
}